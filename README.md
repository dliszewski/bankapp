# BankApp

BankApp is a Spring Boot application built using Gradle.

## Reference Documentation

To build application

`./gradlew clean build`

To run application

`./gradlew bootRun`

To run unit tests

`./gradlew test` 

To run integration tests

`./gradlew integration` 

Application available at address

`http://localhost:8080`

Api documentation generated with swagger

`http://localhost:8080/swagger-ui.html`

## Use cases

- Client can have multiple accounts
- Client can top up account with amount
- Client can transfer money from one account to any other
- Transfers between account accounts of one user are free of charge
- First 5 transfers in billing cycle to other clients accounts are free of charge,
 after it there is additional fee of 2% of transfer amount 
- Billing cycle is one month

## Assumptions made during development

- The unique rule for client is pesel number
- The unique rule for account is account number
- Pesel number must have 11 digits
- Account number must have 26 digits
- TopUp amount must be greater then 0
- Scheduler process to set new date for end of billing cycle runs every minute 
(configuration in application.yml - billing.cron)


## Architecture

This is typical layered architecture with only 2 layers (business and persistence)

### Frameworks and libraries

- spock
- testcontainers - postgresql
- spring boot
- mapstruct
- lombok
- swagger
- h2database
- hibernate
- jpa
- logbook

### Database

- In application is used H2. To see your h2 db `http://localhost:8080/h2`
- In integration tests there are two possibilities:
  - Default one is Postgresql from testcontainers to check against real db. 
  - H2

If you don't have docker on your machine, you can change integration db to H2 (in application-test.yml)
  
### Tests

Application has unit and integration tests. Testing framework is Spock. 