package com.dliszewski.bankapp.domain

import com.dliszewski.bankapp.BankappApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@ActiveProfiles(['test'])
@SpringBootTest(classes = [BankappApplication.class])
class BaseIntegrationSpec extends Specification {

}
