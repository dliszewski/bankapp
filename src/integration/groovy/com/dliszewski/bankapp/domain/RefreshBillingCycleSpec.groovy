package com.dliszewski.bankapp.domain

import com.dliszewski.bankapp.domain.account.dto.AccountDto
import com.dliszewski.bankapp.domain.account.dto.CreateAccountDto
import com.dliszewski.bankapp.domain.account.dto.TransferRequestDto
import com.dliszewski.bankapp.domain.account.repository.AccountRepository
import com.dliszewski.bankapp.domain.account.service.AccountService
import com.dliszewski.bankapp.domain.client.dto.CreateClientDto
import com.dliszewski.bankapp.domain.client.dto.ClientDto
import com.dliszewski.bankapp.domain.client.repository.ClientRepository
import com.dliszewski.bankapp.domain.client.service.ClientService
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired

import java.time.Clock
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset

class RefreshBillingCycleSpec extends BaseIntegrationSpec {

    @Autowired
    ClientService userService

    @Autowired
    AccountService accountService

    @Autowired
    ClientRepository clientRepository

    @Autowired
    AccountRepository accountRepository

    @SpringBean
    private Clock clock = Mock()

    private Wrapper wrapper

    class Wrapper {
        Instant instant
    }

    def mockClock(){
        this.wrapper = new Wrapper()
        clock.instant() >> {
            wrapper.instant
        }
        clock.getZone() >> ZoneOffset.UTC
        return this.wrapper
    }

    void setup() {
        wrapper = mockClock()
        clientRepository.deleteAll()
        accountRepository.deleteAll()
    }

    def "should refresh end of billing cycle"() {
        given: "create account with billingCycleEnd at 2019-06-04 00:00:00.0"
            wrapper.instant = Instant.parse("2019-05-03T06:37:30.00Z")
            ClientDto user1 = userService.createClient(new CreateClientDto("Tomek", "Kowalski", "77111123456"))
            ClientDto user2 = userService.createClient(new CreateClientDto("Bartek", "Koc", "88431343456"))
            AccountDto account1 = accountService.createAccount(new CreateAccountDto("87235000026534798731808353", user1.id))
            AccountDto account2 = accountService.createAccount(new CreateAccountDto("38249000052825956245722466", user2.id))
        and:"Top up account"
            accountService.topUpBalance(account1.id, 50.00)
        and:"Make transfer to second account"
            accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 20.00))
        and:
            accountService.getAccount(account1.id).transfers == 1
        and: "pass time till the end of billing cycle"
            wrapper.instant = Instant.parse("2019-06-04T00:00:01.0Z")
        when: "run scheduler to refresh end of billing cycle and reset transfers count"
            accountService.refreshBillingCycle()
        then: "should set new end of billing cycle for 2019-07-05 00:00:00.0"
            def account = accountService.getAccount(account1.id)
            account.billingCycleEnd == LocalDateTime.of(2019, 7, 5, 0, 0 ,0)
            account.transfers == 0
    }
}
