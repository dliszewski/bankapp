package com.dliszewski.bankapp.domain


import com.dliszewski.bankapp.domain.account.dto.CreateAccountDto
import com.dliszewski.bankapp.domain.account.dto.TransferRequestDto
import com.dliszewski.bankapp.domain.account.exception.InsufficientBalance
import com.dliszewski.bankapp.domain.account.repository.AccountRepository
import com.dliszewski.bankapp.domain.account.service.AccountService
import com.dliszewski.bankapp.domain.client.dto.CreateClientDto
import com.dliszewski.bankapp.domain.client.repository.ClientRepository
import com.dliszewski.bankapp.domain.client.service.ClientService
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired

import java.time.Clock
import java.time.Instant
import java.time.ZoneOffset

class AccountTransferSpec extends BaseIntegrationSpec {

    @Autowired
    ClientService clientService

    @Autowired
    AccountService accountService

    @Autowired
    ClientRepository clientRepository

    @Autowired
    AccountRepository accountRepository

    @SpringBean
    private Clock clock = Mock()

    private Wrapper wrapper

    class Wrapper {
        Instant instant
    }

    def mockClock() {
        this.wrapper = new Wrapper()
        clock.instant() >> {
            wrapper.instant
        }
        clock.getZone() >> ZoneOffset.UTC
        return this.wrapper
    }

    void setup() {
        wrapper = mockClock()
        accountRepository.deleteAll()
        clientRepository.deleteAll()
    }

    def "should make free transfer"() {
        given:
            wrapper.instant = Instant.parse "2019-05-03T06:37:30.00Z"
            def user1 = clientService.createClient(new CreateClientDto("Tomek", "Kowalski", "77111123456"))
            def user2 = clientService.createClient(new CreateClientDto("Bartek", "Koc", "88431343456"))
            def account1 = accountService.createAccount new CreateAccountDto("87235000026534798731808353", user1.id)
            def account2 = accountService.createAccount new CreateAccountDto("38249000052825956245722466", user2.id)
            accountService.topUpBalance(account1.id, 99.99)
        when:
            accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 49.45))
        then:
            accountService.getAccount(account1.id).balance == 50.54
            accountService.getAccount(account2.id).balance == 49.45
    }

    def "should make 5 free transfers and 6th one with additional fee"() {
        given:
            wrapper.instant = Instant.parse("2019-05-03T06:37:30.00Z")
            def user1 = clientService.createClient(new CreateClientDto("Tomek", "Kowalski", "77111123456"))
            def user2 = clientService.createClient(new CreateClientDto("Bartek", "Koc", "88431343456"))
            def account1 = accountService.createAccount new CreateAccountDto("87235000026534798731808353", user1.id)
            def account2 = accountService.createAccount new CreateAccountDto("38249000052825956245722466", user2.id)
            accountService.topUpBalance(account1.id, 1000.00)
        when: "make 5 free transfers"
            accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 100.00))
            accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 100.00))
            accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 100.00))
            accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 100.00))
            accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 100.00))
        and: "make transfer with additional fee"
            accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 100.00))
        then:
            accountService.getAccount(account1.id).balance == 398
            accountService.getAccount(account2.id).balance == 600
    }

    def "should failed to make transfer"() {
        given:
        wrapper.instant = Instant.parse "2019-05-03T06:37:30.00Z"
        def user1 = clientService.createClient(new CreateClientDto("Tomek", "Kowalski", "77111123456"))
        def user2 = clientService.createClient(new CreateClientDto("Bartek", "Koc", "88431343456"))
        def account1 = accountService.createAccount new CreateAccountDto("87235000026534798731808353", user1.id)
        def account2 = accountService.createAccount new CreateAccountDto("38249000052825956245722466", user2.id)
        accountService.topUpBalance(account1.id, 50.00)
        when:
        accountService.makeTransfer(account1.id, new TransferRequestDto(account2.id, 100.00))
        then:
        thrown(InsufficientBalance)
    }

}
