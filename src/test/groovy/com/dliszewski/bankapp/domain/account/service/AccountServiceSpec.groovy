package com.dliszewski.bankapp.domain.account.service

import com.dliszewski.bankapp.domain.account.dto.CreateAccountDto
import com.dliszewski.bankapp.domain.account.dto.TransferRequestDto
import com.dliszewski.bankapp.domain.account.entity.Account
import com.dliszewski.bankapp.domain.account.exception.AccountAlreadyExists
import com.dliszewski.bankapp.domain.account.exception.AccountNotFoundException
import com.dliszewski.bankapp.domain.account.exception.InsufficientBalance
import com.dliszewski.bankapp.domain.account.mapper.AccountMapperImpl
import com.dliszewski.bankapp.domain.account.repository.AccountRepository
import com.dliszewski.bankapp.domain.client.entity.Client
import com.dliszewski.bankapp.domain.client.exception.ClientNotFoundException
import com.dliszewski.bankapp.domain.client.repository.ClientRepository
import spock.lang.Specification

import java.time.Clock
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset

class AccountServiceSpec extends Specification {

    AccountRepository accountRepository = Mock()
    ClientRepository clientRepository = Mock()
    Clock clock = Clock.fixed(LocalDateTime.now().toInstant(ZoneOffset.UTC), ZoneId.systemDefault())
    AccountService accountService = new AccountService(accountRepository, clientRepository, new AccountMapperImpl(), clock)

    def "should throw AccountAlreadyExists when account with the same number already exists in db"() {
        given:
        accountRepository.existsAccountByAccountNumber(_ as String) >> true
        when:
        accountService.createAccount(new CreateAccountDto("87235000026534798731808353", 1L))
        then:
        thrown(AccountAlreadyExists.class)
    }

    def "should throw ClientNotFoundException when client don't exists in db"() {
        given:
        accountRepository.existsAccountByAccountNumber(_ as String) >> false
        clientRepository.findById(_ as Long) >> Optional.empty()
        when:
        accountService.createAccount(new CreateAccountDto("87235000026534798731808353", 1L))
        then:
        thrown(ClientNotFoundException.class)
    }

    def "should throw AccountNotFoundException when client don't exists in db"() {
        given:
        accountRepository.findById(_ as Long) >> Optional.empty()
        when:
        accountService.getAccount(1L)
        then:
        thrown(AccountNotFoundException.class)
    }

    def "should throw AccountNotFoundException when one of account not exists"() {
        given:
        accountRepository.findById(_ as Long) >> Optional.empty()
        when:
        accountService.makeTransfer(1L, new TransferRequestDto())
        then:
        thrown(AccountNotFoundException.class)
    }

    def "should throw AccountNotFoundException when second account don't exists"() {
        given:
        accountRepository.findById(_ as Long) >>> [Optional.of(new Account()), Optional.empty()]
        when:
        accountService.makeTransfer(1L, new TransferRequestDto(1, 1.00))
        then:
        thrown(AccountNotFoundException.class)
    }

    def "should throw InsufficientBalance when there is insufficient balance"() {
        given:
        accountRepository.findById(_ as Long) >>> [
                Optional.of(new Account(balance: 5.00, client: new Client(pesel: "1"))),
                Optional.of(new Account(balance: 0.00, client: new Client(pesel: "2")))
        ]
        when:
        accountService.makeTransfer(1L, new TransferRequestDto(1, 10.00))
        then:
        thrown(InsufficientBalance.class)
    }

    def "should throw AccountNotFoundException when top up balance"() {
        given:
        accountRepository.findById(_ as Long) >> Optional.empty()
        when:
        accountService.topUpBalance(1L, 1.00)
        then:
        thrown(AccountNotFoundException.class)
    }
}
