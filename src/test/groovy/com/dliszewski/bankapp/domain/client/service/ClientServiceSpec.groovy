package com.dliszewski.bankapp.domain.client.service

import com.dliszewski.bankapp.domain.client.dto.CreateClientDto
import com.dliszewski.bankapp.domain.client.exception.ClientAlreadyExists
import com.dliszewski.bankapp.domain.client.exception.ClientNotFoundException
import com.dliszewski.bankapp.domain.client.mapper.ClientMapperImpl
import com.dliszewski.bankapp.domain.client.repository.ClientRepository
import spock.lang.Specification

class ClientServiceSpec extends Specification {

    ClientRepository clientRepository = Mock()
    ClientService clientService = new ClientService(clientRepository, new ClientMapperImpl())

    def "should throw ClientAlreadyExists when client already exists in db"() {
        given:
            clientRepository.existsClientByPesel(_ as String) >> true
        when:
            clientService.createClient(new CreateClientDto("Tom", "Kowalski", "77111123456"))
        then:
            thrown(ClientAlreadyExists.class)
    }

    def "should throw ClientNotFoundException when client not found in db"() {
        given:
        clientRepository.findById(_ as Long) >> Optional.empty()
        when:
        clientService.getClient(1L)
        then:
        thrown(ClientNotFoundException.class)
    }
}
