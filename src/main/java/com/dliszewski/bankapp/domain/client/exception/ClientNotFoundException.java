package com.dliszewski.bankapp.domain.client.exception;

import com.dliszewski.bankapp.config.ParameterizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ClientNotFoundException extends ParameterizedException {
    public ClientNotFoundException(Long id) {
        super(String.format("Client with id %s not found", id), id);
    }
}
