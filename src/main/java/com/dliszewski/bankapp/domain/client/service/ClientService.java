package com.dliszewski.bankapp.domain.client.service;

import com.dliszewski.bankapp.domain.client.dto.CreateClientDto;
import com.dliszewski.bankapp.domain.client.exception.ClientAlreadyExists;
import com.dliszewski.bankapp.domain.client.repository.ClientRepository;
import com.dliszewski.bankapp.domain.client.dto.ClientDto;
import com.dliszewski.bankapp.domain.client.entity.Client;
import com.dliszewski.bankapp.domain.client.exception.ClientNotFoundException;
import com.dliszewski.bankapp.domain.client.mapper.ClientMapper;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@AllArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;

    public ClientDto getClient(Long id) {
        log.info("Getting client with id:  " + id);
        return clientRepository.findById(id)
                .map(clientMapper::clientToClientDto)
                .orElseThrow(() -> new ClientNotFoundException(id));
    }

    @Transactional
    public ClientDto createClient(CreateClientDto createClientDto){
        if (clientRepository.existsClientByPesel(createClientDto.getPesel())) {
            throw new ClientAlreadyExists(createClientDto.getPesel());
        } else {
            Client client = clientMapper.createClientDtoToClient(createClientDto);
            Client saved = clientRepository.save(client);
            return clientMapper.clientToClientDto(saved);
        }
    }

    public List<ClientDto> getAllClients() {
        return clientRepository.findAll().stream()
                .map(clientMapper::clientToClientDto)
                .collect(Collectors.toList());
    }
}
