package com.dliszewski.bankapp.domain.client.exception;

import com.dliszewski.bankapp.config.ParameterizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class ClientAlreadyExists extends ParameterizedException {

    public ClientAlreadyExists(String pesel) {
        super(String.format("Client with pesel number %s already exists", pesel), pesel);
    }
}
