package com.dliszewski.bankapp.domain.client.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ClientDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String pesel;
}
