package com.dliszewski.bankapp.domain.client.mapper;

import com.dliszewski.bankapp.domain.client.dto.ClientDto;
import com.dliszewski.bankapp.domain.client.dto.CreateClientDto;
import com.dliszewski.bankapp.domain.client.entity.Client;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientMapper {
    ClientDto clientToClientDto(Client client);
    Client clientDtoToClient(ClientDto clientDto);
    Client createClientDtoToClient(CreateClientDto userDto);
}
