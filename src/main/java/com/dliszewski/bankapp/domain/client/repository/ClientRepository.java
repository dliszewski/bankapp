package com.dliszewski.bankapp.domain.client.repository;

import com.dliszewski.bankapp.domain.client.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

    boolean existsClientByPesel(String pesel);

}
