package com.dliszewski.bankapp.domain.account.exception;

import com.dliszewski.bankapp.config.ParameterizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class AccountNotFoundException extends ParameterizedException {
    public AccountNotFoundException(Long id) {
        super(String.format("Account with id %s not found", id), id);
    }
}
