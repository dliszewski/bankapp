package com.dliszewski.bankapp.domain.account.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BillingCycleScheduler {

    private final AccountService accountService;

    public BillingCycleScheduler(AccountService accountService) {
        this.accountService = accountService;
    }

    @Scheduled(cron = "${billing.cron}")
    public void scheduleBillingCycleEnd() {
        log.info("refresh billing cycle started...");
        accountService.refreshBillingCycle();
        log.info("refresh billing cycle finished");
    }

}
