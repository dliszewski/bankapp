package com.dliszewski.bankapp.domain.account.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountDto {

    @NotBlank
    @Size(min = 26, max = 26)
    private String accountNumber;

    @NotNull
    private Long userId;

}
