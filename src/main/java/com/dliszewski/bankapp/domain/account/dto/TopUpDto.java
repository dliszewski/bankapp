package com.dliszewski.bankapp.domain.account.dto;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Data;

@Data
public class TopUpDto {

    @NotNull
    @Positive
    private BigDecimal amount;

}
