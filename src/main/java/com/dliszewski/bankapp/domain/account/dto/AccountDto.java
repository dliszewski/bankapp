package com.dliszewski.bankapp.domain.account.dto;

import com.dliszewski.bankapp.domain.client.dto.ClientDto;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class AccountDto {

    private Long id;

    private String accountNumber;

    private ClientDto user;

    private BigDecimal balance;

    private LocalDateTime createdAt;

    private int transfers;

    private LocalDateTime billingCycleEnd;
}
