package com.dliszewski.bankapp.domain.account.entity;

import com.dliszewski.bankapp.domain.client.entity.Client;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "ACCOUNT")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, name = "ID")
    private Long id;

    @Column(unique = true, name = "ACCOUNT_NUMBER")
    private String accountNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLIENT_ID")
    private Client client;

    @Column(name = "BALANCE")
    private BigDecimal balance;

    @Column(name = "CREATED_AT", updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "TRANSFERS")
    private int transfers;

    @Column(name = "BILLING_CYCLE_END")
    private LocalDateTime billingCycleEnd;
}
