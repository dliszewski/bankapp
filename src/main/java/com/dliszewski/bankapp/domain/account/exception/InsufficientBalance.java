package com.dliszewski.bankapp.domain.account.exception;

import com.dliszewski.bankapp.config.ParameterizedException;

public class InsufficientBalance extends ParameterizedException {

    public InsufficientBalance(Long accountId) {
        super("Insufficient balance to make transfer for accountId: " + accountId, accountId);
    }
}
