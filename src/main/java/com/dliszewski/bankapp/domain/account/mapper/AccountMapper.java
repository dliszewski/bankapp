package com.dliszewski.bankapp.domain.account.mapper;

import com.dliszewski.bankapp.domain.account.dto.AccountDto;
import com.dliszewski.bankapp.domain.account.dto.CreateAccountDto;
import com.dliszewski.bankapp.domain.account.entity.Account;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    Account accountDtoToAccount(AccountDto accountDto);
    AccountDto accountToAccountDto(Account accountDto);
    Account createAccountDtoToAccount(CreateAccountDto createAccountDto);
}
