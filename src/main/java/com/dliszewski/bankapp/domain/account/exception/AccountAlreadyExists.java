package com.dliszewski.bankapp.domain.account.exception;

import com.dliszewski.bankapp.config.ParameterizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class AccountAlreadyExists extends ParameterizedException {

    public AccountAlreadyExists(String accountNumber) {
        super(String.format("Account with number %s already exists", accountNumber), accountNumber);
    }
}
