package com.dliszewski.bankapp.domain.account.repository;

import com.dliszewski.bankapp.domain.account.entity.Account;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
    boolean existsAccountByAccountNumber(String accountNumber);
    List<Account> findAllByBillingCycleEndBefore(LocalDateTime now);
}
