package com.dliszewski.bankapp.domain.account.service;

import com.dliszewski.bankapp.domain.account.dto.AccountDto;
import com.dliszewski.bankapp.domain.account.dto.CreateAccountDto;
import com.dliszewski.bankapp.domain.account.dto.TransferRequestDto;
import com.dliszewski.bankapp.domain.account.entity.Account;
import com.dliszewski.bankapp.domain.account.exception.AccountAlreadyExists;
import com.dliszewski.bankapp.domain.account.exception.AccountNotFoundException;
import com.dliszewski.bankapp.domain.account.exception.InsufficientBalance;
import com.dliszewski.bankapp.domain.account.mapper.AccountMapper;
import com.dliszewski.bankapp.domain.account.repository.AccountRepository;
import com.dliszewski.bankapp.domain.client.entity.Client;
import com.dliszewski.bankapp.domain.client.exception.ClientNotFoundException;
import com.dliszewski.bankapp.domain.client.repository.ClientRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@AllArgsConstructor
public class AccountService {

    private static final int FREE_TRANSFERS_LIMIT = 5;
    private final AccountRepository accountRepository;
    private final ClientRepository clientRepository;
    private final AccountMapper accountMapper;
    private final Clock clock;

    @Transactional
    public AccountDto createAccount(CreateAccountDto createAccountDto) {
        if (accountRepository.existsAccountByAccountNumber(createAccountDto.getAccountNumber())) {
            throw new AccountAlreadyExists(createAccountDto.getAccountNumber());
        }

        Client client = clientRepository.findById(createAccountDto.getUserId())
                .orElseThrow(() -> new ClientNotFoundException(createAccountDto.getUserId()));

        LocalDateTime now = LocalDateTime.now(clock);
        Account account = accountMapper.createAccountDtoToAccount(createAccountDto);
        account.setBalance(BigDecimal.ZERO);
        account.setClient(client);
        account.setCreatedAt(now);
        account.setBillingCycleEnd(LocalDateTime.of(now.toLocalDate().plusMonths(1).plusDays(1), LocalTime.MIDNIGHT));
        Account savedAccount = accountRepository.save(account);
        return accountMapper.accountToAccountDto(savedAccount);
    }

    @Transactional
    public AccountDto getAccount(Long id) {
        return accountRepository.findById(id)
                .map(accountMapper::accountToAccountDto)
                .orElseThrow(() -> new AccountNotFoundException(id));
    }

    @Transactional
    public List<AccountDto> getAllAccounts() {
        return accountRepository.findAll().stream()
                .map(accountMapper::accountToAccountDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public void makeTransfer(Long accountId, TransferRequestDto transferRequestDto) {
        Account sourceAccount = accountRepository.findById(accountId)
                .orElseThrow(() -> new AccountNotFoundException(accountId));
        Account targetAccount = accountRepository.findById(transferRequestDto.getAccountToId())
                .orElseThrow(() -> new AccountNotFoundException(transferRequestDto.getAccountToId()));

        sourceAccount.setTransfers(sourceAccount.getTransfers() + 1);

        BigDecimal amountToTransfer = transferRequestDto.getAmount();
        BigDecimal additionalFee = calculateAdditionalFee(sourceAccount, amountToTransfer, targetAccount);
        BigDecimal amountToReduce = amountToTransfer.add(additionalFee);

        if (sourceAccount.getBalance().subtract(amountToReduce).compareTo(BigDecimal.ZERO) < 0) {
            throw new InsufficientBalance(accountId);
        }

        sourceAccount.setBalance(sourceAccount.getBalance().subtract(amountToReduce));
        targetAccount.setBalance(targetAccount.getBalance().add(amountToTransfer));
    }

    private BigDecimal calculateAdditionalFee(Account sourceAccount, BigDecimal amountToTransfer, Account targetAccount) {
        BigDecimal additionalFee = BigDecimal.ZERO;
        if(!accountsFromOneClient(sourceAccount.getClient(), targetAccount.getClient())) {
            log.info("Accounts don't belong to the same client, checking if additional fee should be added");
            if (sourceAccount.getTransfers() > FREE_TRANSFERS_LIMIT) {
                additionalFee = amountToTransfer.multiply(new BigDecimal(2)).divide(new BigDecimal(100), RoundingMode.FLOOR);
                log.info("Adding additional fee " + additionalFee);
            } else {
                log.info("No additional fee for this transfer");
            }
        }
        return additionalFee;
    }

    private boolean accountsFromOneClient(Client sourceClient, Client targetClient) {
        return sourceClient.getPesel().equals(targetClient.getPesel());
    }

    @Transactional
    public void topUpBalance(Long accountId, BigDecimal amount) {
        Account sourceAccount = accountRepository.findById(accountId)
                .orElseThrow(() -> new AccountNotFoundException(accountId));
        log.info(String.format("Account with id: %d topUp with: %s", accountId, amount));
        sourceAccount.setBalance(sourceAccount.getBalance().add(amount));
    }

    @Transactional
    public void refreshBillingCycle() {
        LocalDateTime now = LocalDateTime.now(clock);
        List<Account> accounts = accountRepository.findAllByBillingCycleEndBefore(now);
        accounts.forEach(this::updateBillingCycleAndTransfersCount);
    }

    private void updateBillingCycleAndTransfersCount(Account account) {
        account.setTransfers(0);
        account.setBillingCycleEnd(account.getBillingCycleEnd().plusMonths(1).plusDays(1));
        log.info(String.format("Setting new end of billing cycle for accountId: %d for %s", account.getId(), account.getBillingCycleEnd()));
    }


}
