package com.dliszewski.bankapp.config.logbook;

import javax.annotation.Nullable;
import org.zalando.logbook.BodyFilter;

public class ObfuscatingLogbookBodyFilter implements BodyFilter {

    @Override
    public String filter(@Nullable String contentType, String body) {
        return "-";
    }
}
