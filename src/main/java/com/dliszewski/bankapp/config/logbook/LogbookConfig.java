package com.dliszewski.bankapp.config.logbook;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.logbook.BodyFilter;
import org.zalando.logbook.HeaderFilter;
import org.zalando.logbook.HeaderFilters;

@Configuration
@ConditionalOnProperty(name = "logging.logbook.enabled", havingValue = "true", matchIfMissing = true)
public class LogbookConfig {

    @Bean
    BodyFilter bodyFilter() {
        return new ObfuscatingLogbookBodyFilter();
    }

    @Bean
    HeaderFilter headersFilter() {
        return HeaderFilters.replaceHeaders(any -> true, "-");
    }

}
