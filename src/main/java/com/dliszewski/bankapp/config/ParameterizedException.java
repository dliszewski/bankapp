package com.dliszewski.bankapp.config;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public abstract class ParameterizedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final Object[] params;

    public ParameterizedException(String message, Object... params) {
        super(message);
        this.params = params;
    }

    public Object[] getParams() {
        return params;
    }
}
