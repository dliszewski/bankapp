package com.dliszewski.bankapp.config;

import java.io.Serializable;

public class ErrorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String key;
    private final Object errorCode;
    private final Object[] params;
    private final String message;
    private final String rootCause;

    public ErrorDTO(String key, String message, Object errorCode, Object[] params, String rootCause) {
        this.key = key;
        this.message = message;
        this.errorCode = errorCode;
        this.params = params;
        this.rootCause = rootCause;
    }

    public String getKey() {
        return key;
    }

    public Object[] getParams() {
        return params;
    }

    public String getMessage() {
        return message;
    }

    public String getRootCause() {
        return rootCause;
    }

    public Object getErrorCode() {
        return errorCode;
    }
}
