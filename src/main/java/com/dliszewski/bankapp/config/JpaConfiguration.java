package com.dliszewski.bankapp.config;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.dliszewski.bankapp")
@EnableJpaAuditing(dateTimeProviderRef = "clockDateTimeProvider")
public class JpaConfiguration {

    @Bean
    public DateTimeProvider clockDateTimeProvider(){
        return () -> {
            final LocalDateTime localDateTime = LocalDateTime.now(clock());
            return Optional.of(localDateTime);
        };
    }

    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }
}
