package com.dliszewski.bankapp.config;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public abstract class ParameterizedExceptionWithErrorCode extends ParameterizedException {

    private final Object errorCode;

    public ParameterizedExceptionWithErrorCode(String message, Object errorCode, Object... params) {
        super(message, params);
        this.errorCode = errorCode;
    }

    public Object getErrorCode() {
        return errorCode;
    }
}
