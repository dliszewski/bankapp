package com.dliszewski.bankapp.config;

import java.io.PrintWriter;
import java.io.StringWriter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ParameterizedException.class)
    public ResponseEntity<ErrorDTO> processParameterizedException(ParameterizedException ex) {
        logger.error(ex.getMessage(), ex);
        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
        ResponseEntity.BodyBuilder builder = ResponseEntity.status(responseStatus.code());
        return builder.body(buildErrorResponseBody(ex, ex.getParams()));
    }

    @ExceptionHandler(ParameterizedExceptionWithErrorCode.class)
    public ResponseEntity<ErrorDTO> processParameterizedExceptionWithErrorCode(ParameterizedExceptionWithErrorCode ex) {
        logger.error(ex.getMessage(), ex);
        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
        ResponseEntity.BodyBuilder builder = ResponseEntity.status(responseStatus.code());
        return builder.body(buildErrorResponseBody(ex, ex.getErrorCode(), ex.getParams()));
    }

    protected ErrorDTO buildErrorResponseBody(Exception ex, Object errorCode, Object[] params) {
        logger.error(ex.getMessage(), ex);

        String stackTrace = null;

        if (shouldWriteRootCause()) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            stackTrace = sw.toString();
        }
        return new ErrorDTO(ex.getClass().getSimpleName(), ex.getMessage(), errorCode, params, stackTrace);
    }

    private ErrorDTO buildErrorResponseBody(Exception ex, Object[] params) {
        return buildErrorResponseBody(ex, null, params);
    }

    protected boolean shouldWriteRootCause() {
        return false;
    }


}
