package com.dliszewski.bankapp;

import java.net.InetAddress;
import java.net.UnknownHostException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
@Slf4j
public class BankappApplication {

    public static final String SERVER_PORT = "server.port";

    public static void main(String[] args) throws UnknownHostException {
        Environment env = SpringApplication.run(BankappApplication.class, args).getEnvironment();

        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        String appName = env.getProperty("spring.application.name");

        log.info("\n----------------------------------------------------------\n\t"
                        + "Application '{}' is running! Access URLs:\n\t"
                        + "Local: \t\t{}://localhost:{}/{}/\n\t"
                        + "External: \t{}://{}:{}/{}/\n\t"
                        + "Swagger link: \t\t{}://localhost:{}/{}/{}\n\t"
                        + "\n----------------------------------------------------------\n\t",
                appName,
                protocol,
                env.getProperty(SERVER_PORT),
                appName,
                protocol,
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty(SERVER_PORT),
                appName,
                protocol,
                env.getProperty(SERVER_PORT),
                appName,
                "swagger-ui.html"
        );
    }

}
