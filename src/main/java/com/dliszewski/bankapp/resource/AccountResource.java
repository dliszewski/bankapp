package com.dliszewski.bankapp.resource;

import com.dliszewski.bankapp.domain.account.dto.AccountDto;
import com.dliszewski.bankapp.domain.account.dto.CreateAccountDto;
import com.dliszewski.bankapp.domain.account.dto.TopUpDto;
import com.dliszewski.bankapp.domain.account.dto.TransferRequestDto;
import com.dliszewski.bankapp.domain.account.service.AccountService;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
@AllArgsConstructor
public class AccountResource {

    private final AccountService accountService;

    @GetMapping
    public List<AccountDto> getAccounts() {
        return accountService.getAllAccounts();
    }

    @GetMapping(value = "/{id}")
    public AccountDto getAccount(@PathVariable Long id) {
        return accountService.getAccount(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDto createAccount(@RequestBody @Valid CreateAccountDto createAccountDto) {
        return accountService.createAccount(createAccountDto);
    }

    @PostMapping(value = "/{id}/transfer")
    public void transfer(@PathVariable Long id, @RequestBody @Valid TransferRequestDto transferRequestDto) {
        accountService.makeTransfer(id, transferRequestDto);
    }

    @PostMapping(value = "/{id}/topUp")
    public void topUp(@PathVariable Long id, @RequestBody @Valid TopUpDto topUpDto) {
        accountService.topUpBalance(id, topUpDto.getAmount());
    }

}
